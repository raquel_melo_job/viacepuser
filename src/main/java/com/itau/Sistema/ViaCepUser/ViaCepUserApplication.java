package com.itau.Sistema.ViaCepUser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ViaCepUserApplication {

	public static void main(String[] args) {
		SpringApplication.run(ViaCepUserApplication.class, args);
	}

}
